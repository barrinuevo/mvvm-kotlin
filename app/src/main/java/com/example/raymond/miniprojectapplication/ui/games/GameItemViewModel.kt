package com.example.raymond.miniprojectapplication.ui.games

import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import android.view.View
import com.example.raymond.miniprojectapplication.core.baseviewmodel.BaseViewModel
import com.example.raymond.miniprojectapplication.model.Games
import com.example.raymond.miniprojectapplication.ui.stages.StageListActivity

class GameItemViewModel : BaseViewModel() {
    private val gameName = MutableLiveData<String>()
    private val gameImage = MutableLiveData<String>()

    fun bind(game: Games.Game) {
        gameName.value = game.name
        gameImage.value = "https://www.vazgaming.com/wp-content/uploads/2018/01/Mobile-Legends-GBjhCdYX4u9oRQbdcXx3KK.jpg"
    }

    fun getGameName(): MutableLiveData<String> {
        return gameName
    }

    fun getGameImage(): MutableLiveData<String> {
        return gameImage
    }

    fun onClickListener(view: View) {
        view.context.startActivity(Intent(view.context, StageListActivity::class.java))
    }
}