package com.example.raymond.miniprojectapplication.ui.games

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.raymond.miniprojectapplication.R
import com.example.raymond.miniprojectapplication.databinding.ItemGameBinding
import com.example.raymond.miniprojectapplication.model.Games

class GamesAdapter : RecyclerView.Adapter<GamesAdapter.ViewHolder>() {
    private lateinit var games: List<Games.Game>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GamesAdapter.ViewHolder {
        val binding: ItemGameBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_game, parent, false)
        return ViewHolder(binding).listen { _ , _ ->
            binding.viewModel?.onClickListener(binding.gameImage)
        }
    }

    override fun onBindViewHolder(holder: GamesAdapter.ViewHolder, position: Int) {
        holder.bind(games[position])
    }

    override fun getItemCount(): Int {
        return if (::games.isInitialized) games.size else 0
    }

    fun updateGames(games: List<Games.Game>) {
        this.games = games
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemGameBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = GameItemViewModel()

        fun bind(game: Games.Game) {
            viewModel.bind(game)
            binding.viewModel = viewModel
        }
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(getAdapterPosition(), getItemViewType())
        }
        return this
    }
}