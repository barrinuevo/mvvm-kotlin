package com.example.raymond.miniprojectapplication.ui.stages

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import com.example.raymond.miniprojectapplication.R
import com.example.raymond.miniprojectapplication.databinding.ActivityStageListBinding

class StageListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStageListBinding
    private lateinit var viewModel: StagesViewModel

    private var errorSnackbar: Snackbar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_stage_list)
        binding.stageList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.stageList.layoutManager = GridLayoutManager(this, 2)

        viewModel = ViewModelProviders.of(this).get(StagesViewModel::class.java)
        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })
        binding.viewModel = viewModel
    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }
}