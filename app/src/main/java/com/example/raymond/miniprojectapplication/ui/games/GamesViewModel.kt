package com.example.raymond.miniprojectapplication.ui.games

import android.arch.lifecycle.MutableLiveData
import android.view.View
import android.widget.Toast
import com.example.raymond.miniprojectapplication.R
import com.example.raymond.miniprojectapplication.core.baseviewmodel.BaseViewModel
import com.example.raymond.miniprojectapplication.model.Games
import com.example.raymond.miniprojectapplication.network.GamesApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GamesViewModel : BaseViewModel() {

    @Inject
    lateinit var gamesApi: GamesApi

    private lateinit var subscription: Disposable
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadGames() }

    val gamesAdapter: GamesAdapter = GamesAdapter()

    init {
        loadGames()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun loadGames() {
        subscription = gamesApi.getGames()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrievePostListStart() }
                .doOnTerminate { onRetrievePostListFinish() }
                .subscribe(
                        { result -> onRetrievePostListSuccess(result) },
                        { onRetrievePostListError() }
                )
    }

    private fun onRetrievePostListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePostListSuccess(games: Games) {
        gamesAdapter.updateGames(games._games)
    }

    private fun onRetrievePostListError() {
        errorMessage.value = R.string.post_error
    }

}