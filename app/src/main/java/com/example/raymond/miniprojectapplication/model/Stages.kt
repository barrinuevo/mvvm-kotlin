package com.example.raymond.miniprojectapplication.model

import com.google.gson.annotations.SerializedName

data class Stages(@SerializedName("count") val _count: Int,
                  @SerializedName("next") val _next: String,
                  @SerializedName("previous") val _previous: String,
                  @SerializedName("results") val _stages: ArrayList<Stage>) {

    class Stage(_id: Int, _name: String, _is_active: Boolean) {

        companion object {
            fun create(_id: Int, _name: String, _is_active: Boolean):
                    Stage = Stage(_id, _name, _is_active)
        }

        var id: Int = _id
        var name: String = _name
        var isActive: Boolean = _is_active
    }

}