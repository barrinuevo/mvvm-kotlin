package com.example.raymond.miniprojectapplication.ui.stages

import android.arch.lifecycle.MutableLiveData
import com.example.raymond.miniprojectapplication.core.baseviewmodel.BaseViewModel
import com.example.raymond.miniprojectapplication.model.Stages

class StageItemViewModel : BaseViewModel() {
    private val gameName = MutableLiveData<String>()
    private val gameImage = MutableLiveData<String>()

    fun bind(stage: Stages.Stage) {
        gameName.value = stage.name
        gameImage.value = "http://media.steampowered.com/apps/dota2/posts/meepo_large.jpg"
    }

    fun getStageName(): MutableLiveData<String> {
        return gameName
    }

    fun getStageImage(): MutableLiveData<String> {
        return gameImage
    }
}