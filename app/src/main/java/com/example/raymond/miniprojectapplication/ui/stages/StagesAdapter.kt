package com.example.raymond.miniprojectapplication.ui.stages

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.raymond.miniprojectapplication.R
import com.example.raymond.miniprojectapplication.databinding.ItemStageBinding
import com.example.raymond.miniprojectapplication.model.Stages

class StagesAdapter : RecyclerView.Adapter<StagesAdapter.ViewHolder>() {
    private lateinit var items: List<Stages.Stage>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StagesAdapter.ViewHolder {
        val binding: ItemStageBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_stage, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StagesAdapter.ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return if (::items.isInitialized) items.size else 0
    }

    fun updateStages(games: List<Stages.Stage>) {
        this.items = games
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemStageBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = StageItemViewModel()

        fun bind(game: Stages.Stage) {
            viewModel.bind(game)
            binding.viewModel = viewModel
        }
    }
}