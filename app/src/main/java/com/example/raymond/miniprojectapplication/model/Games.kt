package com.example.raymond.miniprojectapplication.model

import com.google.gson.annotations.SerializedName

data class Games(@SerializedName("count") val _count: Int,
                 @SerializedName("next") val _next: String,
                 @SerializedName("previous") val _previous: String,
                 @SerializedName("results") val _games: ArrayList<Game>) {

    class Game(_id: Int, _name: String, _image: String, _is_active: Boolean) {

        companion object {
            fun create(_id: Int, _name: String, _image: String, _is_active: Boolean):
                    Game = Game(_id, _name, _image, _is_active)
        }

        var id: Int = _id
        var name: String = _name
        var image: String = _image
        var isActive: Boolean = _is_active
    }

}