package com.example.raymond.miniprojectapplication.injection.component

import com.example.raymond.miniprojectapplication.injection.module.NetworkModule
import com.example.raymond.miniprojectapplication.ui.games.GamesViewModel
import com.example.raymond.miniprojectapplication.ui.stages.StagesViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified PostListViewModel.
     * @param postListViewModel PostListViewModel in which to inject the dependencies
     */
    fun inject(postListViewModel: GamesViewModel)
    fun inject(postListViewModel: StagesViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}