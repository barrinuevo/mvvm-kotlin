package com.example.raymond.miniprojectapplication.network

import com.example.raymond.miniprojectapplication.model.Games
import com.example.raymond.miniprojectapplication.model.Stages
import io.reactivex.Observable
import retrofit2.http.GET

interface GamesApi {

    @GET("games")
    fun getGames(): Observable<Games>

    @GET("stages")
    fun getStages(): Observable<Stages>
}