package com.example.raymond.miniprojectapplication.core.baseviewmodel

import android.arch.lifecycle.ViewModel
import com.example.raymond.miniprojectapplication.injection.component.DaggerViewModelInjector
import com.example.raymond.miniprojectapplication.injection.component.ViewModelInjector
import com.example.raymond.miniprojectapplication.injection.module.NetworkModule
import com.example.raymond.miniprojectapplication.ui.games.GamesViewModel
import com.example.raymond.miniprojectapplication.ui.stages.StagesViewModel

abstract class BaseViewModel : ViewModel() {

    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is GamesViewModel -> injector.inject(this)
            is StagesViewModel -> injector.inject(this)
        }
    }
}